# Starter web LP MIAW V2
Ceci est la version finale, qui doit rester privée.
La version publique sera mise à disposition des étudiants.

## Description
Ce projet présente un exemple d'organisation des fichiers SCSS d'un site web suivant le système 7-1.
La norme BEM est utilisée pour le nommage des classes CSS.
Un script basé sur l'installation de paquets par npm est fourni, il permet de compiler les fichiers SCSS et de générer le dossier à mettre en ligne pour déployer le site web.


## Installation
Pour créer un site web à partir de ce dépôt :
1. Cloner ce dépôt avec `git clone git@gitlab.univ-lr.fr:prodrigu/integration-web-projet-structure.git`
2. Se placer dans le dossier contenant le fichier *package.json*
3. Lancer la commande *npm install* qui demande au gestionnaire de paquets d'installer toutes les dépendances listées dans *package.json*
4. Lancer la commande *npm run construire* qui compile les fichiers SCSS, crée le dossier *dist* contenant la version de production du site web, et visualise le site web avec un navigateur


## Ressources requises
- nodejs
- npm


## Remerciements
Merci à Kevin Powell pour son enseignement en intégration web et css.
Merci à 5t3ph pour la base du script de compilation SCSS


